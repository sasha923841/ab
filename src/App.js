import React from 'react'
import Router from '@global/router'

import '@scss/global.scss'

const App = () => {
  return (
        <Router />
  )
}

export default App
