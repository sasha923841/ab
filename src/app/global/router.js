import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'

import Home from '@pages/Home'
import Architecture from '@pages/Architecture'
import ArchitectureSingle from '@pages/ArchitectureSingle'
import About from '@pages/About'

const Router = () => {
  return (
        <BrowserRouter>
                <Switch>
                    <Route path="/about">
                        <About slug={'about'} />
                    </Route>

                    <Route path="/contact">
                        <About slug={'about'} slide={5} />
                    </Route>

                    <Route path="/architecture">
                        <Architecture slug={'architecture'} />
                    </Route>

                   <Route path="/architecture/:slug">
                      <ArchitectureSingle />
                   </Route>

                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
        </BrowserRouter>
  )
}

export default Router
