import React, { useEffect, useState } from 'react'

import PageWrap from '@containers/PageWrap'

import '@scss/archive.scss'

import Img from '@img/gallery-image.png'
import ImgActive from '@img/gallery-image-a.png'
import ImgWhite from '@img/gallery-image-white.png'

const ArchitectureSingle = () => {
  return (<PageWrap>
          <main className='architecture-home'>
              <section id='first-section' className='first-section'>
                  <div className='container'>
                      <div className='box-left'/>
                      <div className='column c-1'>
                          <div className='top'>
                              <div className='content'>
                                  <ul>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <img src={ImgWhite}/>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <div className='bottom'>
                              <span>ОБЩЕСТВЕННЫЕ ЗДАНИЯ</span>
                          </div>
                      </div>
                      <div className='column c-2'>
                          <div className='top'>
                              <div className='content'>
                                  <ul>
                                      <li>
                                          <img src={ImgWhite}/>
                                      </li>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                      <li>
                                          <img src={ImgWhite}/>
                                      </li>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <div className='bottom'>
                              <span>ЖИЛЫЕ ДОМА</span>
                          </div>
                      </div>
                      <div className='column c-3'>
                          <div className='top'>
                              <div className='content'>
                                  <ul>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <img src={ImgWhite}/>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <div className='bottom'>
                              <span>ЧАСТНЫЕ ДОМА</span>
                          </div>
                      </div>
                      <div className='column c-4'>
                          <div className='top'>
                              <div className='content'>
                                  <ul>
                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>

                                      <li>
                                          <img src={ImgWhite}/>
                                      </li>

                                      <li>
                                          <a href='#'>
                                              <span>АПАРТ-ОТЕЛЬ</span>
                                              <img src={Img} className='not-active'/>
                                              <img src={ImgActive} className='active'/>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <div className='bottom'>
                              <span>КОНКУРСЫ</span>
                          </div>
                      </div>
                      <div className='box-right'/>
                  </div>
              </section>
          </main>
      </PageWrap>)
}

export default ArchitectureSingle
