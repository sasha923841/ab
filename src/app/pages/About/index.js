import React, {useEffect, useState, useRef} from 'react'
import PropTypes from 'prop-types'
import {FullPage, Slide} from 'react-full-page'

import PageWrap from '@containers/PageWrap'

import '@scss/about.scss'

const About = (props) => {
   const {slide, slug} = props
   const [content, setContent] = useState({
      title_1: 'ИСТОРИЯ',
      text_1_1: '<p>Студия А-Б была основана в 1994г. московскими архитекторами Андреем' +
         'Савиным, Михаилом Лабазовым и Андреем Чельцовым.</p>' +
         '<p>С момента образования и по настоящее время студия отличается' +
         'разнообразием проектных и архитектурных идей, в основном в современном' +
         'стиле, открывающем возможности для новаторства и нестандартных' +
         'решений.</p>',
      text_1_2: '<ul>' +
         '<li>' +
         'Студия А-Б одинаково серьёзно подходит ко всем проектам, вне зависимости от их объёмов, назначения и значимости.</li>' +
         '<li>Проекты Студии А-Б, неоднократно публиковались в специализированной прессе и получали Архитектурные премии</li>' +
         '<li>Студия А-Б активно занимается дизайном интерьеров жилых и офисных зданий и помещений, участвует в профессиональных и творческих выставках и конкурсах,' +
         'сотрудничает с ведущими российскими и зарубежными компаниями.</li>' +
         '<li>Студия А-Б работает как в Москве, так и в других городах (Пермь, Краснодар, Нижний Новгород), а нашими заказчиками являются российские и зарубежные коммерческие' +
         'структуры и частные лица.</li></ul>',
      title_2: 'УСЛУГИ И РЕШЕНИЯ',
      text_2_1: '<ul>' +
         '<li>АРХИТЕКТУРА</li>' +
         '<li>Функционально-технологичные решения</li>' +
         '<li>Градостроительные концепции</li>' +
         '<li>Управление проектированием</li>' +
         '<li>Интерьерные решения</li>' +
         '<li>Материалы и оборудование</li>' +
         '<li>Конструктивные решения</li>' +
         '<li>Сертификация и проектные услуги</li>' +
         '</ul>',
      title_3: 'ПРИЗНАНИЕ. КОНКУРСЫ',
      text_3_1: '<ul>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '</ul>',
      title_4: 'СОТРУДНИЧЕСТВО И ПАРТНЁРЫ',
      text_4_1: '<ul>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '</ul>',
      title_5: 'ПУБЛИКАЦИИ',
      text_5_1: '<ul>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '<li>NBBJ (США)</li>' +
         '</ul>',
      title_6: 'КОНТАКТЫ',
      phone: '',
      email: '',

   })

   const [about, setAbout] = useState(null)
   const [contact, setContact] = useState(null)

   const [hasError, setErrors] = useState(false)

   const refPage = useRef(null)

   async function fetchData() {
      const api = process.env.REACT_APP_API_KEY + 'pages?slug=' + slug
      // console.log(api)
      const res = await fetch(api)
      res.json()
         .then((res) => {
            if (res.length > 0) {
               setContent(res)
            }
         })
         .catch((err) => setErrors(err))
   }

   useEffect(() => {
      // fetchData()
   }, [])

   const setActiveLink = () => {
      const {activeSlide} = refPage.current.state

      if (activeSlide < 5) {
         setAbout(true)
         setContact(false)
      } else {
         setAbout(false)
         setContact(true)
      }
   }

   return (
      <PageWrap about={about} contact={contact}>
         <main className='about-page'>
            <FullPage ref={refPage} afterChange={setActiveLink} initialSlide={slide}>
               <Slide>
                  <section id='first-section' className='first-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>
                           <div className='top'>
                              <div className='title'>
                                 <h3>{content.title_1}</h3>
                              </div>
                              <div className='text' dangerouslySetInnerHTML={{__html: content.text_1_1}}/>
                           </div>
                           <div className='bottom' dangerouslySetInnerHTML={{__html: content.text_1_2}}/>
                        </div>
                     </div>
                  </section>
               </Slide>

               <Slide>
                  <section id='second-section' className='second-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>

                           <h3>{content.title_2}</h3>

                           <div dangerouslySetInnerHTML={{__html: content.text_2_1}}/>

                        </div>
                     </div>
                  </section>
               </Slide>

               <Slide>
                  <section id='third-section' className='third-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>

                           <h3>{content.title_3}</h3>

                           <div dangerouslySetInnerHTML={{__html: content.text_3_1}}/>

                        </div>
                     </div>
                  </section>
               </Slide>

               <Slide>
                  <section id='fourth-section' className='fourth-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>

                           <h3>{content.title_4}</h3>

                           <div dangerouslySetInnerHTML={{__html: content.text_4_1}}/>

                        </div>
                     </div>
                  </section>
               </Slide>

               <Slide>
                  <section id='fifth-section' className='fifth-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>

                           <h3>{content.title_5}</h3>

                           <div dangerouslySetInnerHTML={{__html: content.text_5_1}}/>

                        </div>

                        <div className='contact-box'>
                           <h4>{content.title_6}</h4>
                           <a href='tel: +7(499) 270-58-44'>{content.phone}</a>
                           <a href={`mailto: ${content.email}`}>{content.email}</a>
                        </div>
                     </div>
                  </section>
               </Slide>

               <Slide>
                  <section id='six-section' className='six-section'>
                     <div className='container'>
                        <div className='box-left'/>
                        <div className='column c-1'/>
                        <div className='column c-2'/>
                        <div className='column c-3'/>
                        <div className='column c-4'/>
                        <div className='box-right'/>

                        <div className='content'>

                           <h4>{content.title_6}</h4>
                           <a href='tel: +7(499) 270-58-44'>{content.phone}</a>
                           <a href={`mailto: ${content.email}`}>{content.email}</a>

                        </div>

                     </div>
                  </section>
               </Slide>
            </FullPage>
         </main>
      </PageWrap>
   )
}

export default About

About.propTypes = {
   slide: PropTypes.number,
   slug: PropTypes.string
}

About.defaultProps = {
   slide: 0
}