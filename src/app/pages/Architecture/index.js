import React, {useEffect, useState} from 'react'
import PropTypes from "prop-types";

import PageWrap from '@containers/PageWrap'

import Img from '@img/gallery-image.png'
import ImgActive from '@img/gallery-image-a.png'
import ImgWhite from '@img/gallery-image-white.png'

import '@scss/archive.scss'


const Architecture = (props) => {
   const {slug} = props
   const [content, setContent] = useState({
      cats: [
         {
            title: 'ОБЩЕСТВЕННЫЕ ЗДАНИЯ',
            content: [
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'white',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
            ],
         },
         {
            title: 'ЖИЛЫЕ ДОМА',
            content: [
               {
                  type: 'white',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'white',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
            ],
         },
         {
            title: 'ЧАСТНЫЕ ДОМА',
            content: [
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'white',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
            ],
         },
         {
            title: 'КОНКУРСЫ',
            content: [
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
               {
                  type: 'white',
               },
               {
                  type: 'photo',
                  title: 'АПАРТ-ОТЕЛЬ',
                  imgFirst: Img,
                  imgSecond: ImgActive,
                  link: '#',
               },
            ],
         }
      ],
   })

   const [hasError, setErrors] = useState(false)

   async function fetchData() {
      const api = process.env.REACT_APP_API_KEY + 'pages?slug=' + slug
      // console.log(api)
      const res = await fetch(api)
      res.json()
         .then((res) => {
            if (res.length > 0) {
               setContent(res)
            }
         })
         .catch((err) => setErrors(err))
   }

   useEffect(() => {
      // fetchData()
   }, [])

   /*Render content*/
   const renderContent = () => {
      const data = content.cats.map((item, i) => {
         <div className={`column c-${i + 1}`} key={`c-${i + 1}`}>
            <div className='top'>
               <div className='content'>
                  <ul>
                     {item.content.map((sItem, i) => {
                        sItem.type == 'photo' ?
                           <li>
                              <a href={sItem.link}>
                                 <span>{sItem.title}</span>
                                 <img src={sItem.imgFirst} className='not-active'/>
                                 <img src={sItem.imgSecond} className='active'/>
                              </a>
                           </li>
                           :
                           <li>
                              <img src={ImgWhite}/>
                           </li>
                     })}
                  </ul>
               </div>
            </div>
            <div className='bottom'>
               <span>{item.title}</span>
            </div>
         </div>
      })

      return data
   }

   return (<PageWrap>
      <main className='architecture-home'>
         <section id='first-section' className='first-section'>
            <div className='container'>
               <div className='box-left'/>
               <div className='column c-1'>
                  <div className='top'>
                     <div className='content'>
                        <ul>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <img src={ImgWhite}/>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div className='bottom'>
                     <span>ОБЩЕСТВЕННЫЕ ЗДАНИЯ</span>
                  </div>
               </div>
               <div className='column c-2'>
                  <div className='top'>
                     <div className='content'>
                        <ul>
                           <li>
                              <img src={ImgWhite}/>
                           </li>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                           <li>
                              <img src={ImgWhite}/>
                           </li>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div className='bottom'>
                     <span>ЖИЛЫЕ ДОМА</span>
                  </div>
               </div>
               <div className='column c-3'>
                  <div className='top'>
                     <div className='content'>
                        <ul>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <img src={ImgWhite}/>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div className='bottom'>
                     <span>ЧАСТНЫЕ ДОМА</span>
                  </div>
               </div>
               <div className='column c-4'>
                  <div className='top'>
                     <div className='content'>
                        <ul>
                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>

                           <li>
                              <img src={ImgWhite}/>
                           </li>

                           <li>
                              <a href='#'>
                                 <span>АПАРТ-ОТЕЛЬ</span>
                                 <img src={Img} className='not-active'/>
                                 <img src={ImgActive} className='active'/>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div className='bottom'>
                     <span>КОНКУРСЫ</span>
                  </div>
               </div>
               <div className='box-right'/>
            </div>
         </section>
      </main>
   </PageWrap>)
}

export default Architecture

Architecture.propTypes = {
   slug: PropTypes.string
}

