import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'

import RunPeoples from '@components/Peoples'

import Loader from '@components/Loader'

import '@scss/home.scss'

import IconLeft from '@img/peoples-left.png'
import IconRight from '@img/peoples-right.png'

const Home = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 })
  const [image, setImage] = useState(IconLeft)

  useEffect(() => {
    addEventListeners()
    return () => removeEventListeners()
  }, [position])

  const addEventListeners = () => {
    document.addEventListener('mousemove', onMouseMove)
  }

  const removeEventListeners = () => {
    document.removeEventListener('mousemove', onMouseMove)
  }

  const onMouseMove = (e) => {
    if (position.x + 45 < e.clientX) {
      setImage(IconRight)
    } else {
      setImage(IconLeft)
    }

    setPosition({ x: e.clientX - 45, y: e.clientY - 45 })
  }

  return (<div className='page-home'>
    <section id='first-section' className='first-section'>
      <div className='container'>
        <div className='left'>
            <Link to='/about'>СТУДИЯ А-Б</Link>
        </div>

        <div className='center'>
          <div className='content'>
            <div className='top'>
              <ul>
                <li>
                  <a href='#'>eng</a>
                </li>
                <li>
                  <a href='#'>рус</a>
                </li>
              </ul>
            </div>

            <div className='bottom'>
              <div className='menu-box'>
                <div className='cursor-box' style={{ left: position.x }}>
                  <RunPeoples image={image} />
                </div>
                <ul>
                  <li>
                    <Link to='/architecture'>АРХИТЕКТУРА</Link>
                  </li>
                  <li>
                    <Link to='/architecture'>ДИЗАЙН</Link>
                  </li>
                  <li>
                    <Link to='/architecture'>ФУТУРОЛОГИЯ</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className='right'>
            <Link to='/contact'>КОНТАКТЫ</Link>
        </div>
      </div>
    </section>
  </div>)
}

export default Home
