import React, { useState, useEffect } from 'react'

import RunPeoples from '@components/Peoples'
import Icon from '@img/peoples-left.png'

const Loader = () => {
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    const numbers = setInterval(startTimer, 15)

    return () => clearInterval(numbers)
  }, [counter])

  const startTimer = () => {
    setCounter(counter => counter + 1)
  }

  if (counter > 100) {
    return null
  } else {
    return <div className='loader'>
          <div className='center'>
              <div className='left'>
                <span>
                   {`${counter}%`}
                </span>
              </div>
              <div className='right'>
                  <RunPeoples />
              </div>
          </div>
      </div>
  }
}

export default Loader
