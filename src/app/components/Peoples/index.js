import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import Icon from '@img/peoples-left.png'

const RunPeoples = (props) => {
  const { image } = props
  const [position, setPosition] = useState(0)

  useEffect(() => {
    const peoples = setInterval(runPeoples, 100)

    return () => clearInterval(peoples)
  }, [position])

  const runPeoples = () => {
    if (position === 108) {
      setPosition(0)
    } else {
      setPosition(position => position + 36)
    }
  }

  return <div className='run-peoples'>
        <img src={image} alt='People Icon' style={{ top: `-${position}px` }}/>
    </div>
}

export default RunPeoples

RunPeoples.propTypes = {
  image: PropTypes.any
}

RunPeoples.defaultProps = {
  image: Icon
}
