import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'

import Header from '@containers/Header'
import Footer from '@containers/Footer'

const PageWrap = (props) => {
  const { children, about, contact } = props

  return (
      <div className='page-wrap'>
        <Header/>
        <div className='left-link'>
            <div className='content'>
                <Link to='/about' className={about ? 'active' : null} >СТУДИЯ А-Б</Link>
            </div>
        </div>
        {children}
        <div className='right-link'>
            <div className='content'>
                <Link to='contact' className={contact ? 'active' : null}>КОНТАКТЫ</Link>
            </div>
        </div>
        <Footer/>
    </div>)
}

export default PageWrap

PageWrap.propTypes = {
  children: PropTypes.element,
  about: PropTypes.bool,
  contact: PropTypes.bool
}
