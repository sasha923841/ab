import React, { useEffect, useState } from 'react'

const Header = () => {
  return (<header>
      <div className='container'>
            <div className='content'>
                  <ul>
                    <li>
                        <a href='#'>eng</a>
                    </li>
                    <li>
                        <a href='#'>рус</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>)
}

export default Header
