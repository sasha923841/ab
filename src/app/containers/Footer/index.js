import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'

import IconLeft from '@img/peoples-left.png'
import IconRight from '@img/peoples-right.png'
import RunPeoples from '@components/Peoples'

const Footer = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 })
  const [image, setImage] = useState(IconLeft)

  useEffect(() => {
    addEventListeners()
    return () => removeEventListeners()
  }, [position])

  const addEventListeners = () => {
    document.addEventListener('mousemove', onMouseMove)
  }

  const removeEventListeners = () => {
    document.removeEventListener('mousemove', onMouseMove)
  }

  const onMouseMove = (e) => {
    if (position.x + 45 < e.clientX) {
      setImage(IconRight)
    } else {
      setImage(IconLeft)
    }

    setPosition({ x: e.clientX - 45, y: e.clientY - 45 })
  }

  return (<footer>
        <div className='container'>
            <div className='cursor-box' style={{ left: position.x }}>
                <RunPeoples image={image} />
            </div>

            <div className='left'>
                <div className='content'></div>
            </div>

            <div className='right'>
                <div className='content'>
                    <div className='menu-container'>
                        <div className='menu-box'>
                            <ul>
                                <li>
                                    <Link to='/architecture'>АРХИТЕКТУРА</Link>
                                </li>
                                <li>
                                    <Link href='#'>ДИЗАЙН</Link>
                                </li>
                                <li>
                                    <Link href='#'>ФУТУРОЛОГИЯ</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>)
}

export default Footer
