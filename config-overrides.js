const { alias } = require('react-app-rewire-alias')

module.exports = function override (config) {

  alias({
    '@containers': 'src/app/containers',
    '@components': 'src/app/components',
    '@global': 'src/app/global',
    '@pages': 'src/app/pages',
    '@scss': 'src/assets/scss',
    '@img': 'src/assets/img'

  })(config)

  return config
}
