System.config({
  paths: {
    '@containers/*': './src/app/containers/*',
    '@components/*': './src/app/components/*',
    '@global/*': './src/app/global/*',
    '@pages/*': './src/app/pages/*',
    '@scss/*': './src/assets/scss/*',
    '@img/*': './src/assets/img/*'
  }
})
